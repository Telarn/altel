__author__ = 'Telarn'
from urllib import request, parse
from bs4 import BeautifulSoup
from settings import altelsettings
import csv

login = altelsettings['login']
pwd = altelsettings['pwd']
authlink = 'https://cabinet.altel.kz/'
user_agent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/' \
             '537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
page_list = []
bal = []


def nice_print(data):
    for item in data:
        print(' '.join(item))


def balance(balance_page):
    if balance_page.find_all(class_='account-balance'):
        bal.append([balance_page.find(class_='account-balance').find(class_='split-title').string,
                    balance_page.find(class_='account-balance').find(class_='account-balance-note').string])
        for i in balance_page.find_all(class_='counter'):

            amount = i.contents[0].strip()
            unit = i.find(class_='counter-title').string
            where = i.find(class_='counter-note').contents[0].strip()
            try:
                expires = i.find(class_='counter-note').contents[1].string
                z = [amount, unit, where, expires]
            except IndexError:
                z = [amount, unit, '',  where]
            bal.append(z)
    else:
        print('Что-то пошло не так.')


def init_cook(link):
    init_cookie = request.urlopen(link).getheader('Set-Cookie')
    return init_cookie


def logining(link):
    details = parse.urlencode({'form_login': login, 'form_pass': pwd}).encode('UTF-8')
    headers = {'User-Agent': user_agent,
               'Cookie': init_cook(link)}
    url = request.Request(link, details, headers)
    balance_page = request.urlopen(url)
    balance(BeautifulSoup(balance_page))
    usefull_cookie = balance_page.getheader('Set-Cookie')
    return usefull_cookie


def page_content(link, cookie):
    headers = {'User-Agent': user_agent,
               'Cookie': cookie}
    url = request.Request(link, headers=headers)
    page = BeautifulSoup(request.urlopen(url).read())
    return page


def page_content_list():
    cookie = logining(authlink)
    page_number = 0
    while True:
        historylink = 'https://cabinet.altel.kz/payment-history?page={}&all'.format(page_number)
        page = page_content(historylink, cookie)
        if page.find(class_='hms-time'):
            page_list.append(page)
            page_number += 1
        else:
            break
    return page_list


def get_result(data):
    details = []
    for i in data:
        payday = i.find(class_='split-title').contents[0]
        payday = payday.replace(' / ', '')
        paytime = i.find(class_='hms-time').string
        payamount = i.contents[-1]
        x = [payday, paytime, payamount]
        details.append(x)
    return details


def reporting():
    soup_list = page_content_list()
    data_list = []
    for soup in soup_list:
        data = soup.find_all(class_='split')[1:]
        data_list.extend(data)
    rep = get_result(data_list)
    return rep

report = reporting()


def csvwrite():
    with open("output.csv", "w", newline='') as f:
        writer = csv.writer(f)
        writer.writerows(bal)
        writer.writerows(report)


if __name__ == '__main__':
    csvwrite()
    pass